using CodeChallenge.Models;
using CodeChallenge.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CodeChallenge.Tests
{
    [TestClass]
    public class RearrangeProcessTest
    {
        private static readonly RearrangeProcessService _rearrangeProcessService = new RearrangeProcessService();

        [TestMethod]
        public void HappyPath()
        {
            _rearrangeProcessService.ReadNames("[\"Name1 Name2 Name3\", \"Name4 Name5 Name6\"]");
            GenericResult result = _rearrangeProcessService.ReadAndProcessOrder("[3, 1, 2]");
            
            Assert.IsTrue(result.Success);
            StringAssert.Contains(result.Message, "Name3 Name1 Name2");
            StringAssert.Contains(result.Message, "Name6 Name4 Name5");
        }

        [TestMethod]
        public void LargeHappyPath()
        {
            _rearrangeProcessService.ReadNames("[\"Name1 Name2 Name3 Name4 Name5 Name6 Name7 Name8\"]");
            GenericResult result = _rearrangeProcessService.ReadAndProcessOrder("[1, 2, 3, 4, 6, 5, 8, 7]");

            Assert.IsTrue(result.Success);
            StringAssert.Contains(result.Message, "Name1 Name2 Name3 Name4 Name6 Name5 Name8 Name7");
        }

        [TestMethod]
        public void SmallHappyPath()
        {
            _rearrangeProcessService.ReadNames("[\"Name1\"]");
            GenericResult result = _rearrangeProcessService.ReadAndProcessOrder("[1]");

            Assert.IsTrue(result.Success);
            StringAssert.Contains(result.Message, "Name1");
        }

        [TestMethod]
        public void NamesValidation()
        {
            GenericResult result = _rearrangeProcessService.ReadNames("[ ]");

            Assert.IsFalse(result.Success);
        }

        [TestMethod]
        public void OrderValidation()
        {
            _rearrangeProcessService.ReadNames("[\"Name1 Name2 Name3\", \"Name4 Name5 Name6\"]");
            GenericResult result = _rearrangeProcessService.ReadAndProcessOrder("[3, 1, 100]");

            Assert.IsFalse(result.Success);
        }
    }
}