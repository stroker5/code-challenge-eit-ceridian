﻿using CodeChallenge.Models;
using CodeChallenge.Services;

namespace CodeChallenge
{
    internal class Program
    {
        private static readonly RearrangeProcessService _rearrangeProcessService = new RearrangeProcessService();

        static void Main(string[] args)
        {
            ReadNames();
        }

        /// <summary>
        /// Call service to read the names from Input
        /// </summary>
        private static void ReadNames()
        {
            Console.WriteLine("Please enter an array of individuals names (E.g. [\"Name1 Name2 Name3\", \"Name4 Name5 Name6\"]).");

            string inputNames = Console.ReadLine();
            GenericResult namesResult = _rearrangeProcessService.ReadNames(inputNames);
            if (namesResult.Success)
            {
                ReadAndProcessOrder();
            }
            else
            {
                Console.WriteLine(namesResult.Message);
                ReadNames();
            }
        }

        /// <summary>
        /// Call service to read, validate and process the Order from Input
        /// </summary>
        private static void ReadAndProcessOrder()
        {
            Console.WriteLine($"Please enter the desired order in array format (E.g. {_rearrangeProcessService.GetSampleArray()}).");
            string inputNames = Console.ReadLine();
            GenericResult namesResult = _rearrangeProcessService.ReadAndProcessOrder(inputNames);
            if (namesResult.Success)
            {
                Console.WriteLine(namesResult.Message);
            }
            else
            {
                Console.WriteLine(namesResult.Message);
                ReadAndProcessOrder();
            }
        }
    }
}
