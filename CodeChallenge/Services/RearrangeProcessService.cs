﻿using CodeChallenge.Models;
using System.Text;

namespace CodeChallenge.Services
{
    public class RearrangeProcessService
    {
        private static List<List<string>> SplitNames = new();
        private static List<int> OrderList = new();

        /// <summary>
        /// Read the names from Input
        /// </summary>
        /// <param name="input">String Input (E.g. ["Name1 Name2 Name3", "Name4 Name5 Name6"])</param>
        /// <returns>Returns a response validating if the process was successful</returns>
        public GenericResult ReadNames(string input)
        {
            GenericResult result = new GenericResult();

            //Validate if input is not empty
            if (string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input))
            {
                result.Message = "Warning: Names cannot be empty.";
                return result;
            }

            //Split input into a nested String List 
            SplitNames = new List<List<string>>();
            List<string> initialList = input.Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(x => CleanElement(x)).ToList();
            foreach (var item in initialList)
            {
                SplitNames.Add(item.Split(' ').Where(x => !string.IsNullOrEmpty(x)).ToList());
            }

            //Validate if SplitNames or any sub element is empty
            if (SplitNames.Count == 0 || SplitNames.Max(x => x.Count) == 0)
            {
                result.Message = "Warning: Invalid input.";
                return result;
            }

            //Validate if any sub element has diferent length
            if (SplitNames.Max(x => x.Count) != SplitNames.Min(x => x.Count))
            {
                result.Message = "Warning: The arrays have different lengths.";
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>
        /// Read, validate and process the Order from Input
        /// </summary>
        /// /// <param name="input">String Input (E.g. [1, 2, 3])</param>
        /// <returns>Returns a response validating if the final process was successful</returns>
        public GenericResult ReadAndProcessOrder(string input)
        {
            GenericResult result = new GenericResult();

            //Validate if input is not empty
            if (string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input))
            {
                result.Message = "Warning: Order cannot be empty.";
                return result;
            }

            //Split and Clean input into a String List 
            List<string> orderFromInput = input.Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(x => CleanElement(x)).ToList();

            int sampleSize = SplitNames.FirstOrDefault()?.Count ?? 0;
            OrderList = new List<int>();

            //Iterate and validate the orderFromInput to add values to the OrderList
            foreach (var item in orderFromInput)
            {
                //Validate and try parse the string into an Int
                bool isValidNumber = int.TryParse(item, out int itemNumber);
                if (!isValidNumber)
                {
                    result.Message = $"Warning: The input '{item}' is not an int number.";
                    return result;
                }

                //Validate if number is less or equal to 0 to avoid errors
                if (itemNumber <= 0)
                {
                    result.Message = "Warning: The input number cannot be less or equal to 0.";
                    return result;
                }

                //Validate if number is greater than the length of the OrderList to avoid errors
                if (itemNumber > sampleSize)
                {
                    result.Message = $"Warning: The input '{itemNumber}' is greater than the length of the array of names.";
                    return result;
                }

                //Validate if number is already in the OrderList
                if (OrderList.Any(x => x == itemNumber))
                {
                    result.Message = $"Warning: The input '{itemNumber}' is already in the number array.";
                    return result;
                }

                OrderList.Add(itemNumber);
            }

            //Validate if OrderList length is different than SplitNames to maintain integrity
            if (OrderList.Count != sampleSize)
            {
                result.Message = $"Warning: The input array length is different than the length of the array of names. Array {OrderList.Count}. Expected {sampleSize}";
                return result;
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("The result of rearranged string is:");

            //Iterate SplitNames
            foreach (var list in SplitNames)
            {
                //Clone the SplitNames List
                List<string> orderedResult = new(list);
                for (int i = 0; i < list.Count; i++)
                {
                    //Insert the element into the result List based in the desired order
                    int order = OrderList[i] - 1;
                    orderedResult[i] = list[order];
                }

                sb.AppendLine(string.Join(" ", orderedResult.ToArray()));
            }

            result.Success = true;
            result.Message = sb.ToString();

            return result;
        }

        /// <summary>
        /// Get a simple Array Sample in string format based in the SplitNames
        /// </summary>
        public string GetSampleArray()
        {
            int sampleSize = SplitNames.FirstOrDefault()?.Count ?? 0;
            List<int> ramdomSample = new List<int>();

            for (int i = 1; i <= sampleSize; i++)
            {
                ramdomSample.Add(i);
            }

            return $"[{string.Join(", ", ramdomSample.ToArray())}]";
        }

        /// <summary>
        /// Clean common Array chars from a string
        /// </summary>
        /// <param name="x">String to clean</param>
        /// <returns>Returns cleaned string</returns>
        private static string CleanElement(string x)
        {
            return x.Replace("[", string.Empty)
                .Replace("(", string.Empty)
                .Replace(")", string.Empty)
                .Replace("]", string.Empty)
                .Replace("'", string.Empty)
                .Replace("\"", string.Empty);
        }
    }
}
